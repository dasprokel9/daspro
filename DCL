package dcl;
import java.util.Scanner;

public class DCL {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String line1 = "...................";
        String line2 = ".####...####..#....";
        String line3 = ".#...#..#.....#....";
        String line4 = ".#...#..#.....#....";
        String line5 = ".####...####..####.";
        String line6 = "...................";
        
        System.out.println("Contoh Masukan : ");
        int b = input.nextInt();
        int k = input.nextInt();
        
        if (b > 0 && k > 0 && k <= 100) {
            for (int i = 0; i < b; i++) {
                System.out.print(".");
                for (int x = 0; x < k; x++) {
                    System.out.print(line1);
                }
                
                System.out.println("");
                System.out.print(".");
                for (int x = 0; x < k; x++) {
                    System.out.print(line2);
                }
                
                System.out.println("");
                System.out.print(".");
                for (int x = 0; x < k; x++) {
                    System.out.print(line3);
                }
                
                System.out.println("");
                System.out.print(".");
                for (int x = 0; x < k; x++) {
                    System.out.print(line4);
                }
                
                System.out.println("");
                System.out.print(".");
                for (int x = 0; x < k; x++) {
                    System.out.print(line5);
                }
                System.out.println("");
                System.out.print(".");
                for (int x = 0; x < k; x++) {
                    System.out.print(line6);
                }
                 System.out.println("");
            }
            
        }
    }
}
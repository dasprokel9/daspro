/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package daspro;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Christiantari
 */
public class Kandang_Ayam {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner input = new Scanner(System.in);
        System.out.println("Program Menentukan Jumlah Kandang Ayam");
        System.out.println("--------------------------------------");
        System.out.println("");
        System.out.println("Keterangan : ");
        System.out.println("* N = jumlah ekor ayam");
        System.out.println("* M = jumlah maksimal ekor ayam dalam satu kandang");
        System.out.print("Masukkan N dan M (pisah dengan spasi) : ");
        String x = input.nextLine();
        
        String[] y = x.split(" ");
        ArrayList <String> z = new ArrayList<String>();
        String jadi = "";
        
        for (int i=0; i<y.length; i++)
            z.add(y[i].trim());
        
        for (int i=0; i<y.length; i++)
            jadi = jadi+z.get(i)+" ";
        
        double N, M;
        N = Double.valueOf(z.get(0));
        M = Double.valueOf(z.get(1));
        
        if(N>=1 && N<=1000000000 && M>=1 && M<=1000000000){
        double hasil = N/M;
        System.out.println("Jumlah kandang ayam yang dibutuhkan : " +Math.round(Math.ceil(hasil)));
        }
    }
}